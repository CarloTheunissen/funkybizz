#include "VUUpAndDown.h"
#include "../../conf/config.h"

VUUpAndDown::VUUpAndDown(){
  tickCount = 0;
}
void VUUpAndDown::tick(){

  tickCount += directionUp ? 1 : -1;
  if(tickCount >= 100 || tickCount<= 0){
    directionUp = !directionUp;
  }

}
int VUUpAndDown::getColor(){
  //return (sin(value / 2.f*PI /25.f + 5) + 1) / 2.f * 255;
  return tickCount / 100.f * 255 ;
}
void VUUpAndDown::paintPixels(Adafruit_NeoPixel& pixel, const float& percentage){
  const int lit = (pixel.numPixels() / 2.f) / 100.f * percentage;

  const float brightNess = min(30 / percentage, 1);
  for(int i = 0; i <= lit; i++){
    uint32_t colorBot = pixel.Color(getColor()*brightNess , (255 - getColor())*brightNess, 0, 0);
    uint32_t colorTop = pixel.Color((255 - getColor())*brightNess, getColor()*brightNess, 0,0 );

    pixel.setPixelColor(i, colorBot);
    pixel.setPixelColor(pixel.numPixels() - i, colorTop);
  }



}
