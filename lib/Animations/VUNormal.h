#ifndef VUNORMAL
#define VUNORMAL
#include "Animation.h"

class VUNormal : public Animation{
public:
  VUNormal();
  virtual void paintPixels(Adafruit_NeoPixel& pixel, const float& percentage);
  void setMayDrop(bool mayDrop);
  void setReverse(bool reverse);

protected:
  int peak;
  bool mayDrop;
  bool reverse;
  int getActualLedIndex(int current);
  const int ledLength;
};
#endif
