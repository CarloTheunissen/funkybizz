#include "VUSpecial.h"


//r = 255, g = 0, b = 159

void VUSpecial::paintPixels(Adafruit_NeoPixel& pixel, const float& percentage){
    const int center = pixel.numPixels() / 2.f;
    const int blue = 159 - 150 / 100.f * min(percentage + 30, 100);
    const int lit = (center-2) / 100.f * percentage;

    for(int i = 0; i < lit; i++){
        pixel.setPixelColor(center - i, 255, 0, blue);
        pixel.setPixelColor(center + i, 255, 0, blue);
    }

    uint32_t colorEnd = pixel.Color(128,0,250);
    uint32_t colorCenter = pixel.Color(128,0,250);

    pixel.setPixelColor(center - lit, colorCenter);
    pixel.setPixelColor(center - lit - 1, colorEnd);

    pixel.setPixelColor(center + lit, colorCenter);
    pixel.setPixelColor(center + lit + 1, colorEnd);

};
