#ifndef VUSPECIAL
#define VUSPECIAL
#include "Animation.h"

class VUSpecial : public Animation{
public:
  virtual void paintPixels(Adafruit_NeoPixel& pixel, const float& percentage);
};
#endif
