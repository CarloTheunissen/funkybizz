#ifndef VUUPANDDOWN
#define VUUPANDDOWN
#include "Animation.h"

class VUUpAndDown : public Animation{
public:
  VUUpAndDown();
  virtual void paintPixels(Adafruit_NeoPixel& pixel, const float& percentage);
  virtual void tick();
private:
  bool directionUp = true;
  int tickCount;
  int getColor();
};
#endif
