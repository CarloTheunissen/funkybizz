#include "VUNormal.h"
#include "../../conf/config.h"

VUNormal::VUNormal() : ledLength(NUM_LEDS){
  peak = 0;
  reverse = true;
}
void VUNormal::setReverse(bool reverse){
  this->reverse = reverse;
}
void VUNormal::setMayDrop(bool newMayDrop){
  mayDrop = newMayDrop;
}
int VUNormal::getActualLedIndex(int index){
  if(reverse){
    return ledLength - index - 1;
  }
  return index;
}

void VUNormal::paintPixels(Adafruit_NeoPixel& strip, const float& percentage){
  int till = static_cast<int>(percentage / 100.f * NUM_LEDS);
  int red_color;
	int blue_color;

	for (int from = 0; from <= till; from++) {
		int startLedRed = floor(NUM_LEDS / 3);
		red_color = from > startLedRed ? (255 / (NUM_LEDS - startLedRed) * (from - startLedRed) ) : 0;
		int endLedBlue = NUM_LEDS - startLedRed;
		blue_color = from < endLedBlue ? ( from < startLedRed ? 255 : ( 255 - ( 255 / (endLedBlue - startLedRed) * (from - startLedRed) ) )): 0;
		strip.setPixelColor(getActualLedIndex(from), strip.Color(red_color, 0, blue_color, 0));
	}

	if (peak <= till) {
		strip.setPixelColor(getActualLedIndex(peak), strip.Color(red_color, 0, blue_color, 0));
		strip.setPixelColor(getActualLedIndex(peak+1), strip.Color(red_color, 0, blue_color, 0));
	} else {
		strip.setPixelColor(getActualLedIndex(peak), strip.Color(0, 150, 0, 0));
		strip.setPixelColor(getActualLedIndex(peak+1), strip.Color(0, 255, 0, 0));
	}

  if (till > peak) {
    peak = (till + 2);
  }

  if (till > 0 && mayDrop) {
    peak--;
  }
  mayDrop = false;
}
