#ifndef ANIMATION
#define ANIMATION

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
class Animation{
public:
  virtual void paintPixels(Adafruit_NeoPixel& pixel, const float& percentage) = 0;
  virtual void tick();
};

#endif
