#include <Adafruit_NeoPixel.h>

class NeoPixel : public Adafruit_NeoPixel{
public:
  NeoPixel(uint16_t n, uint8_t p=6, neoPixelType t=NEO_GRB + NEO_KHZ800);
  NeoPixel(void);
  void reverse();
};
