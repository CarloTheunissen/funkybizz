#include <NeoPixel.h>
NeoPixel::NeoPixel(uint16_t n, uint8_t p=6, neoPixelType t=NEO_GRB + NEO_KHZ800) : Adafruit_NeoPixel(n,p,t){}
NeoPixel::NeoPixel(void){}

void NeoPixel::reverse(){
  const uint8_t half = floor(numPixels()/2.f);
  uint8_t r,g,b,w;
  uint8_t tr,tg,tb,tw;
  setPixelColor(0,0,0,0);
  setPixelColor(numPixels(), 0,0,0);
  for(int i = 1; i<half; i++){
    getPixelColor(i, r, g, b, w);
    getPixelColor(numPixels()-i, tr, tg, tb, tw);

    setPixelColor(i, tr, tg, tr, tw);
    setPixelColor(numPixels()-i, r, g, b, w);
  }
}
