#include <Arduino.h>
#include <NeoPixel.h>
#include <Timer.h>
#include <VUNormal.h>
#include <VUSpecial.h>
#include <VUUpAndDown.h>

#include "../conf/config.h"

int count = 1;
int peak = 0;
bool mayDrop = false;
int ticker = 0;
int animTimer = 0;
int animEffectNum = 0;

NeoPixel strip (NUM_LEDS, LEDPIN, NEO_GRBW + NEO_KHZ800);

Timer t;
VUNormal vuMeter;
VUSpecial vuSpecial;
VUUpAndDown vuUpAndDown;
void setMayDrop() {
	vuMeter.setMayDrop(true);
}

void updateAnimTimer() {
	if (animTimer < 15) {
		animTimer++;
	} else {
		animTimer = 0;
		animEffectNum++;
		if (animEffectNum == 3) {
			animEffectNum = 0;
		}
	}
}

void setup() {
	count = ceil(1023 / NUM_LEDS);

	strip.setBrightness(BRIGHTNESS);
	strip.begin();
	strip.show();
	//strip.setReverse(true);
	Serial.begin(115200);

	t.every(50, setMayDrop);
	t.every(60000, updateAnimTimer);
	// t.every(250, updateAnimTimer);
}

int read = 0;
int mapToArduino(float out){
	return out/ 125.f * (NUM_LEDS + 3) - 3;
}
bool reverse = true;
bool flipAllowed = true;
int last = 0;

void loop() {
	ticker++;

	if (ticker >= 500){
		flipAllowed = true;
		ticker = 0;
	}

	while (!Serial.available()) {delay(1); } //block till serial available

	char buffer[1];
	Serial.readBytes(buffer, 1);
	int out = static_cast<int>( buffer[0] );
	if(out == 1){ //next animation
		animEffectNum++;
		if(animEffectNum > 3){
			animEffectNum = 0;
		}
		return;
	}
	if(out == 2){ //previous animation
		animEffectNum--;
		if(animEffectNum < 0){
			animEffectNum = 2;
		}
		return;
	}
	int till_led = mapToArduino(out);

	if(till_led >= 140 && flipAllowed){
		reverse = !reverse;
		flipAllowed = false;
	}
	strip.clear();

	// vuSpecial.paintPixels(strip, till_led / 150.f * 100);
	// vuUpAndDown.paintPixels(strip, till_led / 150.f * 100);
	// vuUpAndDown.tick();
	//  vuMeter.paintPixels(strip, till_led / 150.f * 100);

	switch(animEffectNum) {
		case 0:
			vuMeter.paintPixels(strip, till_led / 150.f * 100);
			vuMeter.setReverse(reverse);
		break;
		case 1:
			vuSpecial.paintPixels(strip, till_led / 150.f * 100);
		break;
		case 2:
			vuUpAndDown.paintPixels(strip, till_led / 150.f * 100);
			vuUpAndDown.tick();
		break;
		default:
			vuMeter.paintPixels(strip, till_led / 150.f * 100);
		break;
	}

	last = till_led;

	strip.show();

	t.update();
}
